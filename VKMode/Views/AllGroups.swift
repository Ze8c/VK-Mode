//
//  AllGroups.swift
//  VKMode
//
//  Created by Мак on 28.07.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import UIKit

class AllGroups: UITableView {

    
    let groupAdd: [String] = [
        "Pets",
        "Fitness",
        "Food",
        "Drink",
        "Travel",
        "Cars",
        "X-Treme"
    ]
    
    override func numberOfRows(inSection section: Int) -> Int {
        return groupAdd.count
    }
    
    override func cellForRow(at indexPath: IndexPath) -> UITableViewCell? {
        let cell = dequeueReusableCell(withIdentifier: "GroupsCell", for: indexPath) as! GroupsCell
        
        let groupNew = groupAdd[indexPath.row]
        
        
        cell.groupNameLbl.text = groupNew
        // Configure the cell...
        print (groupNew)
        
        return cell
    }
    

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
