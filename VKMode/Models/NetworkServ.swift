//
//  NetworkServ.swift
//  VKMode
//
//  Created by Мак on 30.07.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Foundation


class NetworkServ {
    var tokenVK: String = ""
    var apiMethod: String = "friends.get"
    
    func loadDataVK() {
        
        
        var urlConstructor = URLComponents()
        urlConstructor.scheme = "http"
        urlConstructor.host = "api.vk.com"
        urlConstructor.path = "/methods/\(apiMethod)"
        urlConstructor.queryItems = [
            URLQueryItem(name: "v", value: "5.80"),
            URLQueryItem(name: "access_token", value: tokenVK)
        ]
        guard let url = urlConstructor.url else { return }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print(json)
            }
        }
        
        task.resume()
    }
}
