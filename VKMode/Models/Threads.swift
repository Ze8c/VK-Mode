//
//  Threads.swift
//  VKMode
//
//  Created by Мак on 17.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Foundation


class LoadNewsFromVK: Thread {
    var reqAPI = RequestAPI()

    override func main() {
        while !isCancelled {
            reqAPI.getNews()
            Thread.sleep(forTimeInterval:VKService.shared.refreshPaused)
        }
    }
}
