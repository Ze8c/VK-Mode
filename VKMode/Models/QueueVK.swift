//
//  QueueGlobal.swift
//  VKMode
//
//  Created by Мак on 21.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Foundation

class QueueVK {
    
    let reqAPI = RequestAPI()
    
    func loadNews() {
        DispatchQueue.global().async {
            self.reqAPI.getNews{ (error) in
                if let error = error {
                    print(error)
                    return
                }
            }
        }
        
    }
}
