//
//  UsersFromVK.swift
//  VKMode
//
//  Created by Мак on 06.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Foundation


struct Users: Decodable {
    var id: Int
    var first_name: String
    var last_name: String
    var photo: URL
    
}

