//
//  UseDB.swift
//  VKMode
//
//  Created by Мак on 12.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Foundation
import RealmSwift


class UseDB {
    
    let realm = try! Realm()
    
    func saveIntoBase(_ smElements: [Friends]) {
        
        try! realm.write {
            realm.add(smElements, update: true)
        }
    }

}
