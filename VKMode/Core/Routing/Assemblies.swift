//
//  Assemblies.swift
//  VKMode
//
//  Created by Максим Сытый on 22.12.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import Imperious
import SwiftUI

final class Assemblies: AssemblyResolver {
    typealias EP = EndPoint
    
    private let netService: RequestAPI
    unowned var router: Imperious<Assemblies>!
    var tabTintColor: Color? { .black }
    
    init(netService: RequestAPI) {
        self.netService = netService
    }
    
    func resolve(endPoint: EndPoint) -> AnyView {
        switch endPoint {
        case .launch: return launch()
        case .login: return login()
        case .newsFeed: return newsFeed()
        case .friends: return friends()
        case .groups: return groups()
        case let .photo(id): return photo(id: id)
        case .search: return search()
        }
    }
    
    func reaction(endPoint: EndPoint) -> Imperious<Assemblies>.Reaction {
        switch endPoint {
        case .launch: return .single
        case .login: return .single
        case .newsFeed:
            return .tab(
                Tab(
                    icon: Image(systemName: "doc.plaintext.fill"),
                    selectIcon: Image(systemName: "doc.text.fill"),
                    name: "News"
                )
            )
        case .friends:
            return .tab(
                Tab(
                    icon: Image(systemName: "person.fill"),
                    selectIcon: Image(systemName: "person.crop.circle"),
                    name: "Friends"
                )
            )
        case .groups:
            return .tab(
                Tab(
                    icon: Image(systemName: "personalhotspot"),
                    selectIcon: Image(systemName: "personalhotspot.circle.fill"),
                    name: "Groups"
                )
            )
        case .photo: return .push
        case .search: return .push
        }
    }
    
    private func launch() -> AnyView { AnyView(LaunchView()) }
    
    private func login() -> AnyView {
        AnyView(WebSU(vm: LoginWebVM(netService: netService, router: router)))
    }
    
    private func newsFeed() -> AnyView {
        AnyView(NewsView(vm: NewsVM(netService: netService)))
    }
    
    private func friends() -> AnyView {
        AnyView(FriendsView(FriendsVM(netService: netService, router: router)))
    }
    
    private func groups() -> AnyView {
        AnyView(GroupsView(GroupsVM(netService: netService, router: router)))
    }
    
    private func photo(id: Int) -> AnyView {
        AnyView(PhotosView(vm: PhotosVM(netService: netService, withID: id)))
    }
    
    private func search() -> AnyView {
        AnyView(SearchView(vm: SearchVM(netService: netService)))
    }
}
