//
//  EndPoint.swift
//  VKMode
//
//  Created by Максим Сытый on 22.12.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import Imperious

enum EndPoint: AbsEndPoint {
    
    static var tabs: [EndPoint] { [.newsFeed, .friends, .groups] }
    
    case launch
    case login
    case newsFeed
    case friends
    case groups
    case photo(Int)
    case search
}
