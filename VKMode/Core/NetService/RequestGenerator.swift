//
//  RequestGenerator.swift
//  VKMode
//
//  Created by Mak on 05.12.2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Combine
import WebKit

enum RequestGenerator {
    
    enum GroupType {
        case get(userID: Int)
        case search(name: String)
        case join(groupID: Int)
        case leave(groupID: Int)
        
        var path: String {
            switch self {
            case .get(_): return "get"
            case .search(_): return "search"
            case .join(_): return "join"
            case .leave(_): return "leave"
            }
        }
    }
    
    //MARK: -Main Parameters
    case friend(userID: Int)
    case group(GroupType)
    case login
    case news(numberPost: Int, startFrom: Int)
    case photo(userID: Int)
}

//MARK: -RequestGeneration
extension RequestGenerator: NetRoutes {
    
    //MARK: -RestAPI settings
    //TODO: Current version API VK
    private var versionAPI: String {
        "5.126"
    }
    
    //MARK: -Creating parameters
    var host: String {
        switch self {
        case .group, .friend, .news, .photo:
            return "https://api.vk.com"
        case .login:
            return "http://oauth.vk.com"
        }
    }
 
    var path: String {
        var res = ""
        
        switch self {
        case .friend: res = "/method/friends.get"
        case let .group(type): res = "/method/groups." + type.path
        case .login: res = "/authorize"
        case .news: res = "/method/newsfeed.get"
        case .photo: res = "/method/photos.getAll"
        }
        
        return res
    }
 
    var method: URLRequest.HTTPMethod {
        switch self {
        case .group, .friend, .news, .photo:
            return .get
        case .login:
            return .post
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var queryItems: [String: String?]? {
        var dict: [String: String?]? = ["v": versionAPI]
        
        switch self {
        case let .friend(value):
            dict?.merge([
                "access_token": MyAccount.token,
                "user_id": "\(value)",
                "list_id": "",
                "order": "hints",
                "count": "10",
                "offset": "",
                "fields": "sex,city,domain,photo_50",
                "name_case": "nom"
            ], uniquingKeysWith: { current, _ in current })
        case let .group(type):
            switch type {
            case let .get(value):
                dict?.merge([
                    "access_token": MyAccount.token,
                    "user_id": "\(value)",
                    "extended": "1"
                ], uniquingKeysWith: { current, _ in current })
            case let .search(value):
                dict?.merge([
                    "access_token": MyAccount.token,
                    "q": value
                ], uniquingKeysWith: { current, _ in current })
            case let .join(value):
                dict?.merge([
                    "access_token": MyAccount.token,
                    "group_id": "\(value)"
                ], uniquingKeysWith: { current, _ in current })
            case let .leave(value):
                dict?.merge([
                    "access_token": MyAccount.token,
                    "group_id": "\(value)"
                ], uniquingKeysWith: { current, _ in current })
            }
        case .login:
            //TODO: Access to some sorces
            let accessToUser: String = "friends,photos,audio,wall,offline,groups"
            
            dict?.merge([
                "client_id": "6646947", //TODO: Application ID in VK
                "redirect_uri": "https://oauth.vk.com/blank.html",
                "response_type": "token",
                "scope": accessToUser
            ], uniquingKeysWith: { current, _ in current })
        case let .news(number, start):
            dict?.merge([
                "access_token": MyAccount.token,
                "filters": "post,photo",
                "return_banned": "0",
                "count": "\(number)",
                "start_from": "\(start)"
            ], uniquingKeysWith: { current, _ in current })
        case let .photo(value):
            dict?.merge([
                "access_token": MyAccount.token,
                "owner_id": "\(value)"
            ], uniquingKeysWith: { current, _ in current })
        }
        
        return dict
    }
    
    var body: Data? { nil }
}
