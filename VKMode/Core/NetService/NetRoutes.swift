//
//  NetRoutes.swift
//  VKMode
//
//  Created by Максим Сытый on 11.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import WebKit

protocol NetRoutes {
    var path: String { get }
    var method: URLRequest.HTTPMethod { get }
    var queryItems: [String: String?]? { get }
    var body: Data? { get }
    var headers: [String: String]? { get }
    var host: String { get }
}

extension NetRoutes {
    var request: URLRequest {
        var constructor = URLComponents(string: host)
        
        if !path.isEmpty {
            constructor?.path = path
        }
        
        if let queryElements = queryItems {
            constructor?.queryItems = queryElements.map({ item in
                URLQueryItem.init(name: item.key, value: item.value)
            })
        }
        
        var urlRequest = URLRequest(url: constructor?.url ?? URL(fileURLWithPath: ""))
        urlRequest.method = method
        
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpBody = body
        
        return urlRequest
    }
}
