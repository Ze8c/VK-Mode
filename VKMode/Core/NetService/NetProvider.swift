//
//  NetProvider.swift
//  VKMode
//
//  Created by Максим Сытый on 11.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import Combine
import WebKit

struct NetProvider<NetRout> where NetRout: NetRoutes {
    private let firebaseSource: WorkFirebase
    private let manager: URLSession
    private let netQueue: DispatchQueue
    private let decoder: JSONDecoder
    
    init() {
        firebaseSource = WorkFirebase()
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30
        manager = URLSession(configuration: sessionConfig)
        
        netQueue = DispatchQueue(label: "background.request.net", qos: .background)
        
        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    func request(_ rout: NetRout) -> URLRequest { rout.request }
    
    func task<Out>(_ rout: NetRout) -> AnyPublisher<Out, Error> where Out: Decodable {
        manager.dataTaskPublisher(for: rout.request)
            .map(\.data)
            .decode(type: ResponseDTO<Out>.self, decoder: decoder)
            .map(\.response)
            .subscribe(on: netQueue)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    func task<Out>(_ rout: NetRout) async throws -> Out where Out: Decodable {
        try await withCheckedThrowingContinuation { next in
            self.manager.dataTask(
                with: rout.request,
                completionHandler: self.resultHandler(complete: next.resume)
            )
        }
    }
    
    func resultHandler<Out>(
        complete: @escaping (Result<Out, Error>) -> Void
    ) -> (
        Data?, URLResponse?, Error?
    ) -> Void where Out: Decodable {
        { data, response, error in
            
            if let error = error {
                complete(.failure(error))
                return
            }
            
            guard let data = data
            else {
                complete(.failure(NSError(
                    domain: "Network ERROR",
                    code: -1,
                    userInfo: ["Response": response, "Data": data])))
                return
            }
            
            do {
                let model = try self.decoder.decode(Out.self, from: data)
                complete(.success(model))
            } catch {
                #if DEBUG
                print(error)
                #endif
                complete(.failure(error))
            }
        }
    }
}
