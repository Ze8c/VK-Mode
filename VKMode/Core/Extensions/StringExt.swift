//
//  StringExt.swift
//  VKMode
//
//  Created by Mak on 05.12.2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

extension String {
    
    var URLEscapedString: String {
        return addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
    
    subscript(bundle bundle: Bundle, default defaultValue: String = "") -> String {
        get {
            return NSLocalizedString(
                self,
                tableName: nil,
                bundle: bundle,
                value: defaultValue,
                comment: "")
        }
    }
}
