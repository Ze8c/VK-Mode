//
//  ColorExt.swift
//  VKMode
//
//  Created by Mak on 17.12.2019.
//  Copyright © 2019 Max. All rights reserved.
//

import UIKit

extension CGColor {
    
    static func getFromUIColor(_ color: UIColor) -> CGColor {
        color.cgColor
    }
}

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: a/100)
    }
}
