//
//  URLRequestExt.swift
//  VKMode
//
//  Created by Mak on 05.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import WebKit

extension URLRequest {
    
    enum HTTPMethod: String {
        case connect = "CONNECT"
        case delete = "DELETE"
        case get = "GET"
        case head = "HEAD"
        case options = "OPTIONS"
        case patch = "PATCH"
        case post = "POST"
        case put = "PUT"
        case trace = "TRACE"
    }
    
    var method: HTTPMethod {
        get { HTTPMethod(rawValue: httpMethod ?? "") ?? .get }
        set { httpMethod = newValue.rawValue }
    }
}
