//
//  ImageCache.swift
//  Motion
//

import UIKit
import SwiftUI

enum CachePolicy {
    case custom(ImageCache)
    case none
    case standart
    
    var cache: ImageCache? {
        switch self {
        case let .custom(imageCache): return imageCache
        case .none: return .none
        case .standart: return TemporaryImageCache()
        }
    }
}

protocol ImageCache {
    subscript(_ url: URL) -> UIImage? { get set }
}

struct TemporaryImageCache: ImageCache {
    
    private let cache = NSCache<NSURL, UIImage>()
    
    subscript(_ key: URL) -> UIImage? {
        get { cache.object(forKey: key as NSURL) }
        set { newValue == nil ? cache.removeObject(forKey: key as NSURL) : cache.setObject(newValue!, forKey: key as NSURL) }
    }
}

struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = TemporaryImageCache()
}

