//
//  AsyncImage.swift
//  Motion
//

import SwiftUI

struct AsyncImage: View {
    
    @ObservedObject private var loader: ImageLoader
    private let placeholder: AnyView
    private let configuration: (Image) -> Image
    
    init<PHolder>(
        url: URL?,
        cachePolicy: CachePolicy = .none,
        placeholder: PHolder,
        configuration: @escaping (Image) -> Image = { $0.resizable() }
    ) where PHolder: View {
        
        loader = ImageLoader(url: url, cache: cachePolicy.cache)
        self.placeholder = AnyView(placeholder)
        self.configuration = configuration
    }
    
    init(
        url: URL?,
        cachePolicy: CachePolicy = .none,
        configuration: @escaping (Image) -> Image = { $0.resizable() }
    ) {
        self.init(
            url: url,
            cachePolicy: cachePolicy,
            placeholder: EmptyView(),
            configuration: configuration
        )
    }
    
    var body: some View {
        image
            .onAppear(perform: loader.load)
            .onDisappear(perform: loader.cancel)
    }
    
    private var image: some View {
        Group {
            if let img = loader.image {
                configuration(Image(uiImage: img))
            } else {
                placeholder
            }
        }
    }
}
