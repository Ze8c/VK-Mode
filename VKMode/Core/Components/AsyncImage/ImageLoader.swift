//
//  ImageLoader.swift
//  Motion
//

import Combine
import UIKit

class ImageLoader: ObservableObject {
    
    @Published var image: UIImage?
    
    private(set) var isLoading = false
    
    private let url: URL?
    private var cache: ImageCache?
    private var cancellable: AnyCancellable?
    
    private let imageProcessingQueue: DispatchQueue
    
    init(url: URL?, cache: ImageCache? = nil) {
        self.url = url
        self.cache = cache
        self.imageProcessingQueue = DispatchQueue(
            label: "image-processing",
            qos: .background,
            attributes: .concurrent)
    }
    
    deinit {
        cancellable?.cancel()
    }
    
    func load() {
        guard let url = url, !isLoading else { return }

        if let image = cache?[url] {
            self.image = image
            return
        }
        
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .handleEvents(
                receiveSubscription: { [weak self] _ in self?.onStart() },
                receiveOutput: { [weak self] in self?.cache($0) },
                receiveCompletion: { [weak self] _ in self?.onFinish() },
                receiveCancel: { [weak self] in self?.cancel() })
            .subscribe(on: imageProcessingQueue)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }
    
    func cancel() {
        isLoading = false
        cancellable?.cancel()
    }
    
    private func onStart() {
        isLoading = true
    }
    
    private func onFinish() {
        isLoading = false
    }
    
    private func cache(_ image: UIImage?) {
        guard let url = url else { return }

        image.map { cache?[url] = $0 }
    }
}
