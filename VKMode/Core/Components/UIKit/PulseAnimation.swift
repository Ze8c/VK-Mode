//
//  PulseAnimation.swift
//  VKMode
//
//  Created by Анна Егорова on 03.02.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import UIKit

final class PulseAnimation: CALayer {
    
    private var animationGroup = CAAnimationGroup()
    
    var radius: CGFloat {
        get { cornerRadius }
        set {
            bounds = CGRect(x: 0, y: 0, width: newValue * 2, height: newValue * 2)
            cornerRadius = newValue
        }
    }
    
    var animationDuration: TimeInterval {
        get { animationGroup.duration }
        set { animationGroup.duration = newValue }
    }
    
    var numberOfPulses: Float {
        get { animationGroup.repeatCount }
        set { self.animationGroup.repeatCount = newValue }
    }
    
    init(position: CGPoint) {
        super.init()
        
        self.position = position
        self.backgroundColor = .getFromUIColor(.black)
        self.contentsScale = UIScreen.main.scale
        self.opacity = 0
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start() {
        DispatchQueue.global(qos: .default).async {
            self.setupAnimationGroup()
            DispatchQueue.main.async {
                self.add(self.animationGroup, forKey: "pulse")
            }
        }
    }
    
    private func setupAnimationGroup() {
        let timingFunction = CAMediaTimingFunction(name: .default)
        self.animationGroup.timingFunction = timingFunction
        self.animationGroup.animations = [scaleAnimation(), createOpacityAnimation()]
        self.animationGroup.timingFunction = CAMediaTimingFunction(name: .default)
    }
    
    private func scaleAnimation() -> CABasicAnimation {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
        
        scaleAnimation.fromValue = 0
        scaleAnimation.toValue = 1
        scaleAnimation.duration = animationDuration
        
        return scaleAnimation
    }
    
    private func createOpacityAnimation() -> CAKeyframeAnimation {
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.keyTimes = [0, 0.3, 1]
        opacityAnimation.values = [0.4, 0.8, 0]
        opacityAnimation.duration = animationDuration
        
        return opacityAnimation
    }
}
