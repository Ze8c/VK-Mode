//
//  CirclePulsation.swift
//  VKMode
//
//  Created by Анна Егорова on 03.02.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import UIKit

final class CirclePulsation: UIView {
    
    private let insideCircle: CALayer
    private let pulse: PulseAnimation
    
    init(size: CGFloat) {
        
        let sizeInsideCircle: CGFloat = size / 2
        
        self.insideCircle = {
            $0.frame.size = CGSize(width: sizeInsideCircle, height: sizeInsideCircle)
            $0.cornerRadius = sizeInsideCircle / 2
            $0.backgroundColor = .getFromUIColor(.init(r: 118, g: 69, b: 239, a: 100))
            return $0
        }(CALayer())
        
        self.pulse = {
            $0.animationDuration = 2.0
            $0.numberOfPulses = .greatestFiniteMagnitude
            $0.radius = sizeInsideCircle
            $0.backgroundColor = .getFromUIColor(.init(r: 118, g: 69, b: 239, a: 100))
            return $0
        }(PulseAnimation(position: .zero))
        
        super.init(frame: CGRect(origin: .zero, size: CGSize(width: size, height: size)))
        
        layer.addSublayer(insideCircle)
        layer.insertSublayer(pulse, below: layer)
        layer.cornerRadius = size / 2
        clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initContent() {
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        insideCircle.pin.center()
        pulse.pin.center()
        pulse.start()
    }
}
