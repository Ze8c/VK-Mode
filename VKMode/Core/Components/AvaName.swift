//
//  AvaName.swift
//  VKMode
//
//  Created by Максим Сытый on 04.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import SwiftUI

struct AvaName: View {
    
    static func make(id: Binding<Int?>) -> (OwnerDTO) -> AnyView {
        { model in
            AnyView(AvaName(model).onTapGesture { id.wrappedValue = model.box().id })
        }
    }
    
    private let model: OwnerDTO
    
    var body: some View {
        HStack(alignment: .top, spacing: 10) {
            AsyncImage(url: model.box().photo, cachePolicy: .standart, placeholder: Text("Loading..."))
                .frame(width: 70, height: 70)
                .cornerRadius(40)
                .padding([.leading, .vertical], 15)
            Text(model.box().name)
                .foregroundColor(.black)
                .font(.system(size: 20))
                .multilineTextAlignment(.leading)
                .padding([.trailing, .vertical], 15)
        }
    }
    
    init(_ model: OwnerDTO) {
        self.model = model
    }
}
