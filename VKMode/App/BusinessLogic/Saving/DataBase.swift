//
//  DataBase.swift
//  VKMode
//
//  Created by Мак on 15/02/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

final class DataBase {
    
    private let manager: String
    
    init() {
        manager = "FileManager"
    }
    
    func deleteAllRecords() {
        
        print("LOG > All DB records erased")
    }
    
    func save(_ items: [String]) {
        
    }
    
    //MARK: - My Friends
    func loadMyFriends() -> Array<OwnerDTO> {
        []
    }
    
    func observerMyFriends(complition: @escaping (Bool) -> Void) {
        
    }
    
    //MARK: - Friend Photo
    func loadFriendPhoto(for id: String) -> Array<PhotoDTO> {
        []
    }
    
    func observerFriendPhoto(
        for id: Int,
        complition: @escaping (Bool) -> Void
    ) {
        
    }
    
    //MARK: - My Groups
    func loadMyGroups() -> Array<OwnerDTO> {
        []
    }
    
    func  deleteGroupFromDB(_ element: OwnerDTO) {
        
    }
    
    func observerMyGroups(complition: @escaping (Bool) -> Void) {
        
    }
}
