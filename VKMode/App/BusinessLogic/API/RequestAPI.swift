//
//  RequestAPI.swift
//  VKMode
//
//  Created by Мак on 12.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import Combine
import WebKit

final class RequestAPI {
    
    private let provider: NetProvider<RequestGenerator>
    
    var vkAuthRequest: URLRequest {
        provider.request(.login)
    }
    
    init() {
        provider = NetProvider<RequestGenerator>()
    }
    
    func getFriends(byUserID id: Int) -> AnyPublisher<[OwnerDTO], Never> {
        provider.task(.friend(userID: id))
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
    
    func getPhoto(byUserID id: Int) -> AnyPublisher<[PhotoDTO], Never> {
        provider.task(.photo(userID: id))
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
    
    func getGroups(byUserID id: Int) -> AnyPublisher<[OwnerDTO], Never> {
        provider.task(.group(.get(userID: id)))
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
    
    func searchGroup(name: String) -> AnyPublisher<[OwnerDTO], Never> {
        provider.task(.group(.search(name: name)))
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
    
    func groupJoin(id: Int) -> AnyPublisher<Int, Never> {
        provider.task(.group(.join(groupID: id)))
            .replaceError(with: 0)
            .eraseToAnyPublisher()
    }
    
    func groupLeave(id: Int) -> AnyPublisher<Int, Never> {
        provider.task(.group(.leave(groupID: id)))
            .replaceError(with: 0)
            .eraseToAnyPublisher()
    }
    
    func getNews(numberPost count: Int) -> (Int) -> AnyPublisher<PostsDTO, Never> {
        { [unowned self] number in
            provider.task(.news(numberPost: count, startFrom: number))
                .replaceError(with: .zero)
                .eraseToAnyPublisher()
        }
    }
}
