//
//  VKModeApp.swift
//  VKMode
//
//  Created by Максим Сытый on 22.12.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import Imperious
import SwiftUI

@main
struct VKModeApp: App {
    
    private let assembler: Assemblies
    private let router: Imperious<Assemblies>
    
    var body: some Scene {
        WindowGroup {
            router.startView()
        }
//        .onChange(of: scenePhase) { phase in
//            switch phase {
//            case .active: print("App is active")
//            case .inactive: print("App is inactive")
//            case .background: print("App is in background")
//            @unknown default: print("Oh - interesting: I received an unexpected new value.")
//            }
//        }
    }
    
    init() {
        assembler = Assemblies(netService: RequestAPI())
        router = Imperious.make(startPoint: .launch, from: assembler)
        assembler.router = router
    }
}
