//
//  AppDelegate.swift
//  VKMode
//
//  Created by Максим on 18.07.2018.
//  Copyright © 2018 Max. All rights reserved.
//

/*import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        #if DEBUG
        print(NSTemporaryDirectory())
        #endif
        
        showStartVC()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
}

private extension AppDelegate {
    
    func showStartVC() {
        DispatchQueue.main.async {
            let loginVC = VKLoginController()
            let mainView = TabNavigator()
            
            self.window = self.makeWindow(with: mainView, animated: true)
            mainView.present(loginVC, animated: false)
        }
    }
    
    func makeWindow(with rootVC: UIViewController, animated: Bool) -> UIWindow {
        
        func set(vc: UIViewController, in window: UIWindow) {
            window.rootViewController = vc
            window.makeKeyAndVisible()
        }
        
        let window: UIWindow = {
            $0.backgroundColor = UIColor.black
            $0.isUserInteractionEnabled = true
            return $0
        }(UIWindow(frame: UIScreen.main.bounds))
        
        guard rootVC != window.rootViewController else { return window }
        
        guard animated else {
            set(vc: rootVC, in: window)
            return window
        }
        
        guard let snapshot = window.snapshotView(afterScreenUpdates: true) else {
            set(vc: rootVC, in: window)
            return window
        }
        
        rootVC.view.addSubview(snapshot)
        set(vc: rootVC, in: window)
        UIView.animate(withDuration: 0.15, animations: {
            snapshot.layer.opacity = 0
        }) { _ in
            snapshot.removeFromSuperview()
        }
        
        return window
    }
}*/
