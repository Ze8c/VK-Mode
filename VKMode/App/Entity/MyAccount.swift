//
//  MyAccount.swift
//  VKMode
//
//  Created by Мак on 15/02/2019.
//  Copyright © 2019 Max. All rights reserved.
//

import Foundation

class MyAccount {
    
    /*static var token: String {
        get { KeychainWrapper.standard.string(forKey: "myToken") ?? "" }
        set { KeychainWrapper.standard.set(newValue, forKey: "myToken") }
    }*/
    static var token: String {
        get { UserDefaults.standard.string(forKey: "myToken") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "myToken") }
    }
    
    static var id: String {
        get { UserDefaults.standard.string(forKey: "id") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "id") }
    }
}
