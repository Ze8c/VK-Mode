//
//  ResponseContainer.swift
//  VKMode
//
//  Created by Максим Сытый on 06.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import Foundation

struct ResponseDTO<Model>: Decodable where Model: Decodable {
    enum CodingKeys: String, CodingKey {
        case response
        case items
        case profiles
    }
    
    let response: Model
    
    init(from decoder: Decoder) throws {
        let box = try decoder.container(keyedBy: CodingKeys.self)
        
        if let result = try? box.decode(Model.self, forKey: .response) {
            response = result
        } else {
            let responseBox = try box.nestedContainer(keyedBy: CodingKeys.self, forKey: .response)
            response = try responseBox.decode(Model.self, forKey: .items)
        }
    }
}
