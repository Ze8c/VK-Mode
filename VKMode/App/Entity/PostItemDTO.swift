//
//  PostItemDTO.swift
//  VKMode
//
//  Created by Максим Сытый on 10.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import Foundation

struct PostItemDTO: Decodable, Hashable {
    private enum CodingKeys: String, CodingKey {
        case postId
        case sourceId
        case postType
        case likes
        case reposts
        case comments
        case views
        case count
        case text
        case attachments
        case photo
        case sizes
        case url
    }
    
    let id: Int
    let ownerId: Int
    let postType: String
    let likes: Int
    let reposts: Int
    let comments: Int
    let views: Int
    let text: String
    let photoURL: URL?
    
    /*init(json: JSON) {
        self.photoURL = URL(string: json["attachments"][0]["photo"]["sizes"][2]["url"].string ?? "")
     }*/
    
    init(from decoder: Decoder) throws {
        let box = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try box.decode(Int.self, forKey: .postId)
        ownerId = try box.decode(Int.self, forKey: .sourceId)
        postType = try box.decode(String.self, forKey: .postType)
        text = try box.decode(String.self, forKey: .text)
        photoURL = URL(string: "")
        
        let likesBox = try box.nestedContainer(keyedBy: CodingKeys.self, forKey: .likes)
        likes = try likesBox.decode(Int.self, forKey: .count)
        
        let repostsBox = try box.nestedContainer(keyedBy: CodingKeys.self, forKey: .reposts)
        reposts = try repostsBox.decode(Int.self, forKey: .count)
        
        let commentsBox = try box.nestedContainer(keyedBy: CodingKeys.self, forKey: .comments)
        comments = try commentsBox.decode(Int.self, forKey: .count)
        
        let viewsBox = try box.nestedContainer(keyedBy: CodingKeys.self, forKey: .views)
        views = try viewsBox.decode(Int.self, forKey: .count)
    }
}
