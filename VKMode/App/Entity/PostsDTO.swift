//
//  PostsDTO.swift
//  VKMode
//
//  Created by Mak on 10.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Foundation

struct PostsDTO: Decodable, Hashable {
    static var zero: PostsDTO { PostsDTO(items: [], profiles: [], groups: []) }
    
    let items: [PostItemDTO]
    let profiles: [OwnerDTO]
    let groups: [OwnerDTO]
}
