//
//  PhotoDTO.swift
//  VKMode
//
//  Created by Mak on 10.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Foundation

struct PhotoDTO: Decodable, Hashable {
    
    private enum CodingKeys: String, CodingKey {
        case ownerId
        case sizes
    }
    
    let ownerId: Int
    private let photoString: String
    
    init(from decoder: Decoder) throws {
        let box = try decoder.container(keyedBy: CodingKeys.self)
        ownerId = try box.decode(Int.self, forKey: .ownerId)
        
        let photos = try box.decode([MediaFileDTO].self, forKey: .sizes)
        photoString = photos.sorted(by: { $0.height > $1.height }).first?.url ?? ""
    }
    
    func photo() -> URL? { URL(string: photoString) }
}
