//
//  OwnerDTO.swift
//  VKMode
//
//  Created by Mak on 10.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Foundation

enum OwnerDTO: Decodable, Hashable {
    
    struct Model: Decodable, Hashable {
        
        fileprivate enum CodingKeys: String, CodingKey {
            case lastName
            case firstName
            case name
            case id
            case photo50
        }
        
        let id: Int
        let name: String
        let photoString: String
        
        var photo: URL? { URL(string: photoString) }
        
        init(from decoder: Decoder) throws {
            let box = try decoder.container(keyedBy: CodingKeys.self)
            
            if let item = try? box.decode(String.self, forKey: .name) {
                name = item
            } else {
                let firstName = try box.decode(String.self, forKey: .firstName)
                let lastName = try box.decode(String.self, forKey: .lastName)
                name = firstName + " " + lastName
            }
            
            id = try box.decode(Int.self, forKey: .id)
            photoString = try box.decode(String.self, forKey: .photo50)
        }
    }
    
    case friend(Model)
    case group(Model)
    
    init(from decoder: Decoder) throws {
        let box = try decoder.container(keyedBy: Model.CodingKeys.self)
        
        if (try? box.decode(String.self, forKey: .name)) != nil {
            self = .group(try Model(from: decoder))
        } else {
            self = .friend(try Model(from: decoder))
        }
    }
    
    func box() -> Model {
        switch self {
        case let .friend(model): return model
        case let .group(model): return model
        }
    }
}
