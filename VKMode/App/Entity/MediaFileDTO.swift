//
//  MediaFileDTO.swift
//  VKMode
//
//  Created by Максим Сытый on 07.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

struct MediaFileDTO: Decodable, Hashable {
    let width: Int
    let height: Int
    let url: String
    let type: String
}
