//
//  PostModel.swift
//  VKMode
//
//  Created by Максим Сытый on 10.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import Foundation

struct PostModel: Hashable {
    static func make(fromDTO model: PostsDTO) -> [PostModel] {
        model.items.map { it in
            let postOwner = (it.ownerId > 0 ? model.profiles : model.groups)
                .first { abs(it.ownerId) == $0.box().id }!
            
            return PostModel(fromPostDTO: it, ownerDTO: postOwner)
        }
    }
    
    let ownerName: String
    let ownerAva: URL?
    let lrcvText: String
    let text: String
    let photo: URL?
    
    private init(fromPostDTO post: PostItemDTO, ownerDTO owner: OwnerDTO) {
        ownerName = owner.box().name
        ownerAva = owner.box().photo
        lrcvText = "💙 \(post.likes) ♻️ \(post.reposts) 💬 \(post.comments) 👁 \(post.views)"
        text = post.text
        photo = post.photoURL
    }
}
