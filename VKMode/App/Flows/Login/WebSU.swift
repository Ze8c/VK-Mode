//
//  WebSU.swift
//  VKMode
//
//  Created by Максим Сытый on 04.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import SwiftUI
import WebKit

public struct WebSU: UIViewRepresentable {
    
    open class Coordinator: NSObject, ObservableObject, WKNavigationDelegate {
        open var request: URLRequest
        
        init(request: URLRequest) {
            self.request = request
        }
    }

    private let vm: Coordinator
    private let webView: WKWebView
    
    public init(vm: Coordinator) {
        self.vm = vm
        webView = WKWebView()
    }
    
    public func makeCoordinator() -> Coordinator { vm }
    
    public func makeUIView(context: Context) -> WKWebView {
        webView.navigationDelegate = context.coordinator
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
        return webView
    }
    
    public func updateUIView(_ webView: WKWebView, context: Context) {
        webView.load(context.coordinator.request)
    }
}
