//
//  LoginWebVM.swift
//  VKMode
//
//  Created by Максим Сытый on 04.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import Combine
import Imperious
import WebKit

final class LoginWebVM: WebSU.Coordinator {
    private let router: Imperious<Assemblies>
    
    init(netService: RequestAPI, router: Imperious<Assemblies>) {
        self.router = router
        
        super.init(request: netService.vkAuthRequest)
        print("INIT >", self)
    }
    
    deinit { print("DEINIT >", self) }
    
    func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationResponse: WKNavigationResponse,
        decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void
    ) {
        
        if parsing(navigationResponse.response) {
            decisionHandler(.cancel)
            Task { await router.dispatch(.newsFeed) }
        } else {
            decisionHandler(.allow)
        }
    }
    
    private func parsing(_ response: URLResponse) -> Bool {
        
        guard let url = response.url,
              url.path == "/blank.html",
              let fragment = url.fragment
        else {
            return false
        }
        
        DispatchQueue.global().async {
            let params = fragment
                .components(separatedBy: "&")
                .map({ $0.components(separatedBy: "=") })
                .reduce(into: [String: String]()) { result, param in
                    result[param[0]] = param[1]
                }
            
            if let uID = params["user_id"], let token = params["access_token"] {
                MyAccount.token = token
                MyAccount.id = uID
            }
        }
        
        return true
    }
}
