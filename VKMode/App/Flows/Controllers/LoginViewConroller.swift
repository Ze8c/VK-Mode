//
//  LoginViewConroller.swift
//  VKMode
//
//  Created by Максим on 18.07.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import UIKit

class LoginViewConroller: UIViewController {
    
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBAction func unwindToLogin (segue: UIStoryboardSegue) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override  func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        
       
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: .UIKeyboardWillShow,
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(notification:)),
            name: .UIKeyboardWillHide,
            object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        let user = usernameField.text ?? ""
        let passw = passwordField.text ?? ""

        
        if (user == "") && (passw == "") {
            return true
        } else {
            return false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
        
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        print(usernameField.text ?? "no username")
        print(passwordField.text ?? "no password")
    }
    
    @IBAction func tappedView(_ sender: Any) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification){
 
        
        guard let info = notification.userInfo as? NSDictionary, let value = info.value(forKey: UIKeyboardFrameEndUserInfoKey) as? NSValue else { return }
        
        let height = value.cgRectValue.size.height
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
        
    }
    
    @objc func keyboardWillHide(notification: Notification){
        print(#function)
        
        scrollView.contentInset = .zero
    }
}
