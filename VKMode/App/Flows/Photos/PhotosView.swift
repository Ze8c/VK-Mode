//
//  PhotosView.swift
//  VKMode
//
//  Created by Mak on 05.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import SwiftUI

struct PhotosView: View {
    
    @ObservedObject var vm: PhotosVM
    
    var body: some View {
        ZStack {
            Color(.orange)
                .edgesIgnoringSafeArea(.vertical)
            
            if vm.items.isEmpty {
                Spiner()
            } else {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .center, spacing: 15) {
                        ForEach(vm.items, id: \.self) { it in
                            AsyncImage(
                                url: it.photo(),
                                cachePolicy: .standart,
                                placeholder: Rectangle().foregroundColor(.white)
                            )
                                .frame(width: 250, height: 250)
                                .cornerRadius(20)
                        }
                    }
                }
            }
        }
    }
}
