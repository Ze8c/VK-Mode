//
//  PhotosVM.swift
//  VKMode
//
//  Created by Mak on 05.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Combine
import Foundation

final class PhotosVM: ObservableObject {
    
    @Published var items: [PhotoDTO] = []
    
    init(netService: RequestAPI, withID id: Int) {
        netService.getPhoto(byUserID: id)
            .assign(to: &$items)
        
        print("INIT >", self)
    }
    
    deinit {
        print("DEINIT >", self)
    }
}
