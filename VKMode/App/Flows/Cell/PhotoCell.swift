//
//  PhotoCell.swift
//  VKMode
//
//  Created by Мак on 24.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {

    @IBOutlet weak var lrcv: UILabel! {
        didSet {
            lrcv.translatesAutoresizingMaskIntoConstraints = false
        }
    } //l - likes, r - reposts, c - comments, v - views
    @IBOutlet weak var name: UILabel! {
        didSet {
            name.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var avatar: UIImageView! {
        didSet {
            avatar.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var photoPost: UIImageView! {
        willSet {
            photoPost.translatesAutoresizingMaskIntoConstraints = false
        }
    }

}
