//
//  FriendsVM.swift
//  VKMode
//
//  Created by Mak on 06.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Combine
import Imperious

final class FriendsVM: ObservableObject {
    
    var friends: [OwnerDTO] = []
    @Published var selectedId: Int?
    
    private var box = Set<AnyCancellable>()
    
    init(netService: RequestAPI, router: Imperious<Assemblies>) {
        Just(0)
            .compactMap { _ in Int(MyAccount.id) }
            .flatMap(netService.getFriends(byUserID:))
            .sink { [weak self] items in
                self?.friends = items
                self?.objectWillChange.send()
            }
            .store(in: &box)
        
        $selectedId
            .compactMap { $0 }
            .sink { id in Task { [weak router] in await router?.dispatch(.photo(id)) } }
            .store(in: &box)
        
        print("INIT >", self)
    }
    
    deinit {
        box.forEach { $0.cancel() }
        print("DEINIT >", self)
    }
}
