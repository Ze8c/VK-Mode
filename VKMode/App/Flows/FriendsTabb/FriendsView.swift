//
//  FriendsView.swift
//  VKMode
//
//  Created by Максим Сытый on 04.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import SwiftUI

struct FriendsView: View {
    @ObservedObject private var vm: FriendsVM
    
    var body: some View {
        List {
            ForEach(vm.friends, id: \.self, content: AvaName.make(id: $vm.selectedId))
        }
        .navigationBarHidden(true)
    }
    
    init(_ vm: FriendsVM) {
        self.vm = vm
    }
}
