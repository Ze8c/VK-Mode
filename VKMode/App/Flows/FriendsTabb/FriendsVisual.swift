//
//  FriendsVisual.swift
//  VKMode
//
//  Created by Mak on 06.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import UIKit
import PinLayout

protocol FriendsVisual: UIView {
    var list: UITableView { get }
}

extension FriendsVisual {
    
    func initViews() {
        backgroundColor = .white
        
        list.pin(to: self).all(pin.safeArea)
    }
}

final class FriendsVisualImpl: UIView, FriendsVisual {
    
    let list: UITableView = {
        $0.register(classCell: AvaNameCell.self)
//        $0.rowHeight = UITableView.automaticDimension
        return $0
    }(UITableView())
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        initViews()
    }
    
    deinit {
        print("DEINIT >", self)
    }
}
