//
//  NewsView.swift
//  VKMode
//
//  Created by Mak on 05.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import SwiftUI
import Accelerate

struct NewsView: View {
    
    @ObservedObject var vm: NewsVM
    
    var body: some View {
        if vm.isLoading {
            Spiner()
        } else {
            List {
                ForEach(vm.posts, id: \.self) { it in
                    PostCell(model: it)
                        .onAppear(perform: self.vm.nextNews(it))
                }
            }
            .navigationBarHidden(true)
        }
    }
}
