//
//  NewsVM.swift
//  VKMode
//
//  Created by Mak on 05.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Combine

final class NewsVM: ObservableObject {
    
    private let netService: RequestAPI
    
    @Published var posts: Array<PostModel> = []
    @Published private var counter: Int = 0
    
    private let numberPost: Int = 15
    var isLoading: Bool = false
    
    private var box = Set<AnyCancellable>()
    
    init(netService: RequestAPI) {
        self.netService = netService
        
        print("INIT >", self)
        
        $counter
            .flatMap(netService.getNews(numberPost: numberPost))
            .map(PostModel.make(fromDTO:))
            .sink { [weak self] it in
                self?.isLoading = false
                self?.posts.append(contentsOf: it)
            }
            .store(in: &box)
    }
    
    deinit {
        box.forEach { $0.cancel() }
        print("DEINIT >", self)
    }
    
    func nextNews(_ it: PostModel) -> () -> Void {
        { [weak self] in
            guard let self = self, self.posts.last == it else { return }
            self.isLoading = true
            self.counter += self.numberPost
        }
    }
}
