//
//  NewsCell.swift
//  VKMode
//
//  Created by Мак on 17.08.2018.
//  Copyright © 2018 Max. All rights reserved.
//

import SwiftUI

struct PostCell: View {
    
    let model: PostModel
    
    private let inset: CGFloat = 5.0
    private let avatarSide: CGFloat = 50
    private let photoPostSide: CGFloat = 130
    
    var body: some View {
        VStack(spacing: inset) {
            header(model)
            
            Divider()
                .foregroundColor(.gray)
                .padding(.horizontal, inset)
            
            content(model)
        } //VStack
            .background(Color.white)
            .cornerRadius(20)
            .shadow(color: .gray, radius: 5, x: 1, y: 1)
            .padding(.vertical, inset)
    }
    
    private func header(_ it: PostModel) -> some View {
        HStack(alignment: .top, spacing: inset) {
            VStack(alignment: .leading, spacing: inset) {
                Text(it.ownerName)
                    .multilineTextAlignment(.leading)
                    .font(.title)
                    .foregroundColor(.black)
                
                Text(it.lrcvText)
                    .multilineTextAlignment(.leading)
                    .font(.system(size: 12))
                    .foregroundColor(.gray)
            } //VStack
            .padding([.leading, .top], 10)
            
            Spacer()
            
            AsyncImage(
                url: it.ownerAva,
                cachePolicy: .standart,
                placeholder: Rectangle().foregroundColor(.white)
            )
                .frame(width: avatarSide, height: avatarSide, alignment: .trailing)
                .cornerRadius(avatarSide / 2)
                .padding([.trailing, .vertical], 10)
        } //HStack
    }
    
    private func content(_ it: PostModel) -> some View {
        VStack {
            Text(it.text)
                .multilineTextAlignment(.leading)
                .font(.system(size: 12))
                .padding(.bottom, 5)
            
            if it.photo != nil {
                AsyncImage(
                    url: it.photo,
                    cachePolicy: .standart,
                    placeholder: Rectangle().foregroundColor(.white)
                )
                    .frame(width: photoPostSide, height: photoPostSide, alignment: .center)
            }
        } //VStack
            .padding(.all, 10)
    }
}
