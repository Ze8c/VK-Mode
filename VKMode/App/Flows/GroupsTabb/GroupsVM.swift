//
//  GroupsVM.swift
//  VKMode
//
//  Created by Mak on 06.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Combine
import Foundation
import Imperious

final class GroupsVM: ObservableObject {
    
    var groups: [OwnerDTO] = []
    private let netService: RequestAPI
    private let router: Imperious<Assemblies>
    
    private var box = Set<AnyCancellable>()
    
    init(netService: RequestAPI, router: Imperious<Assemblies>) {
        self.netService = netService
        self.router = router
        
        Just(0)
            .compactMap { _ in Int(MyAccount.id) }
            .flatMap(netService.getGroups(byUserID:))
            .sink { [weak self] items in
                self?.groups = items
                self?.objectWillChange.send()
            }
            .store(in: &box)
        print("INIT >", self)
    }
    
    deinit {
        box.forEach { $0.cancel() }
        print("DEINIT >", self)
    }
    
    func searchNewGroup() {
        Task { await router.dispatch(.search) }
    }
    
    func deleteItem(at indexSet: IndexSet) {
        guard let idx = indexSet.first else { return }
        
        let group = groups.remove(at: idx)
//        groups.remove(atOffsets: indexSet)
        objectWillChange.send()
        
        Just(group.box().id)
            .flatMap(netService.groupLeave(id:))
            .sink { if $0 == 1 { print("REMOVE") }}
            .store(in: &box)
    }
}
