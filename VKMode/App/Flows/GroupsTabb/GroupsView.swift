//
//  GroupsView.swift
//  VKMode
//
//  Created by Максим Сытый on 04.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import SwiftUI

struct GroupsView: View {
    @ObservedObject private var vm: GroupsVM
    
    var body: some View {
        VStack(spacing: 15) {
            HStack {
                Spacer()
                Button(action: vm.searchNewGroup) {
                    Image(systemName: "plus.circle")
                        .resizable()
                }
                .frame(width: 30, height: 30)
                .padding([.top, .trailing], 20)
            }
            List {
                ForEach(vm.groups, id: \.self, content: AvaName.init)
                    .onDelete(perform: vm.deleteItem(at:))
            }
        }
        .navigationBarHidden(true)
    }
    
    init(_ vm: GroupsVM) {
        self.vm = vm
    }
}
