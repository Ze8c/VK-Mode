//
//  SearchVM.swift
//  VKMode
//
//  Created by Mak on 06.06.2020.
//  Copyright © 2020 Max. All rights reserved.
//

import Combine

final class SearchVM: ObservableObject {
    
    @Published var searchStr: String = ""
    @Published var searchResult: [OwnerDTO] = []
    @Published var selectedId: Int?
    
    private let netService: RequestAPI
    private var box = Set<AnyCancellable>()
    
    init(netService: RequestAPI) {
        self.netService = netService
        
        $searchStr
            .removeDuplicates()
            .flatMap(netService.searchGroup(name:))
            .assign(to: &$searchResult)
        
        $selectedId
            .compactMap { $0 }
            .flatMap(netService.groupJoin(id:))
            .sink { if $0 == 1 { print("DEBUG > Group JOIN") }}
            .store(in: &box)
        
        print("INIT >", self)
    }
    
    deinit {
        print("DEINIT >", self)
    }
}
