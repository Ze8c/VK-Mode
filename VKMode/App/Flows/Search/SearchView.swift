//
//  SearchView.swift
//  VKMode
//
//  Created by Максим Сытый on 06.01.2022.
//  Copyright © 2022 Max. All rights reserved.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject private var vm: SearchVM
    
    var body: some View {
        VStack(spacing: 10) {
            TextField("Search", text: $vm.searchStr)
                .frame(idealWidth: .infinity, maxWidth: .infinity)
                .padding([.top, .horizontal], 15)
            
            List {
                ForEach(vm.searchResult, id: \.self, content: AvaName.make(id: $vm.selectedId))
            }
        }
    }
    
    init(vm: SearchVM) {
        self.vm = vm
    }
}
