//
//  LaunchView.swift
//  VKMode
//
//  Created by Максим Сытый on 22.12.2021.
//  Copyright © 2021 Max. All rights reserved.
//

import Imperious
import SwiftUI

struct LaunchView: View {
    @EnvironmentObject private var router: Imperious<Assemblies>
    
    var body: some View {
        ZStack {
            Color("backgroundLaunch")
                .edgesIgnoringSafeArea(.all)
            
            Text("Launch...")
                .foregroundColor(.white)
                .font(.largeTitle)
        }
        .onAppear { router.dispatch(.login) }
    }
}
